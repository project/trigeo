<?php

// opt is the field name.
function mytrigeo_data($opt,$value) {
  
  $data = array(
    
    'mytrigeo_distance_unit' => array(
      0 => t('km'),
      1 => t('miles')
    ),
    'mytrigeo_cost_currency' => array(
      0 => t('&#8364;'),
      1 => t('&#36;')
    ),

    'month' => array(
      '01' => t('January'), 
      '02' => t('February'), 
      '03' => t('March'), 
      '04' => t('April'), 
      '05' => t('May'), 
      '06' => t('June'), 
      '07' => t('July'), 
      '08' => t('August'),  
      '09' => t('September'), 
      '10' => t('October'), 
      '11' => t('November'), 
      '12' => t('December')
    ),
  );
  
  if( is_numeric($value) ) {
    return $data[$opt][$value];
  }
  else if($value == 'all' ) {
    return $data[$opt];
  }
 
  
}
