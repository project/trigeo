$(document).ready(function(){ 
  
  //alert( $("#edit-trip-has-start").val() + ' x ' + $("#edit-trip-has-end").val() );
  
  
  
  if( $("#edit-trip-has-start").val() == 1 ) {
    $("#display-start").css("display","none");  
  }
  if( $("#edit-trip-has-end").val() == 1 ) {
    $("#display-end").css("display","none");  
  }
  
  $("#display-start fieldset").css("margin","0");
  $("#display-end fieldset").css("margin","0");
  
  
  
  var start = false;
  var end = false;
  

  
  $("#edit-trip-has-start").change(function() {
    if(start) {
      $("#display-start").css("display","none");
      start=false;
    }
    else {
      $("#display-start").fadeIn("slow");
      start=true;
    }
  });
  $("#edit-trip-has-end").change(function() {
    if(end) {
      $("#display-end").css("display","none");
      end=false;
    }
    else {
      $("#display-end").fadeIn("slow");
      end=true;
    }
  });
  
  //alert('hello');
});