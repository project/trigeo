if (GBrowserIsCompatible()) {

  var cancella;
  var cancella_poly=null;

  function createMarker(point,html,icona) 
    {
    
    var ico = new GIcon();
    if(icona !='empty')
    {
      ico.iconSize = new GSize(40,34);
      ico.iconAnchor = new GPoint(20,34);
      ico.infoWindowAnchor = new GPoint(20,3);
      ico.image = document.location.pathname + icona;
      //ico.image = 'http://localhost/trigeo57/files/images/bici.png';
      //ico.image = 'files/category_pictures/bici.png';
      
    }else
      ico=null;
      
    var marker = new GMarker(point,ico);
      
    if(html != 'empty'){
          GEvent.addListener(marker, "click", function() {
            marker.openInfoWindowHtml(html);
          });
        }
    
    return marker;
    }
  
  function print_plt2(plt_coord,html,plt_invert,icona)
  {
    var tratto = [];
    //var rows = [], coordinate = [];
    var count = 0;
    var poly = '';
    
    for(var i=0;i<plt_coord.length;i++)
    {
      
      
      
      var rows = plt_coord[i].split("_");
      for(var j=0;j<rows.length;j++)
      {
        var coord = rows[j].split(",");
        
        var lat = parseFloat(coord[0]);
        var lon = parseFloat(coord[1]);
        
        tratto[count] = new GLatLng(lat ,lon);
        count++;
      }
      
      
        
      
    }
    tratto[0]=tratto[1];
    
    if(plt_invert == 0)
      cancella = createMarker(tratto[0],html,icona);
    else if(plt_invert == 1)
    {
      cancella = createMarker(tratto[tratto.length-1],html,icona);
    }
    
    
    //controllo se lo zoom � ad un livello
    //appropriato 
    
    
    
    map.addOverlay(cancella);
    cancella_poly = new GPolyline(tratto);
    map.addOverlay(cancella_poly);
    
  
  
    
  }
  
  function find_xml_path(nomehook)
  {
    var str = document.location;
    //aggiunto un chr per problemi di casting
    str+=' ';
    if (str.search("/?q=")==-1)
    {
      //calcolo le dim del nome della pagina corrente
      //per rimuoverlo con un substr 
      var split = str.split("/");
      var cut = split[split.length-1];
      var size = cut.length;
      //aggiungo lo spazio vuoto
      size++;
      var limit = str.length - size;
      //alert(str.substr(0,limit));
      
      out =   str.substr(0,limit) + '/' + nomehook;
    }else
    {
      out = document.location.pathname + '?q=' + nomehook;
    }
    
    return out;
  }
  
  //legge da un file XML informazioni per la creazione di una mappa
    function marker_from_rssview(path,is_child) { 
   // Read the data from example.xml
    
    
    var request = GXmlHttp.create();
    request.open("GET", path, true);
    request.onreadystatechange = function() {
    if (request.readyState == 4) {
      var xmlDoc = GXml.parse(request.responseText);
     
      //seleziono <geo>
      var markers = xmlDoc.documentElement.getElementsByTagName("geo");
      //seleziono il tag con il plt
      var plt_coord = xmlDoc.documentElement.getElementsByTagName("plt_coord");
      //seleziono il tag item
      var leggi =  xmlDoc.documentElement.getElementsByTagName("item");
      //seleziono il tag channel
      var channel =  xmlDoc.documentElement.getElementsByTagName("channel");
      
      var read_link_name = GXml.value(channel[0].getElementsByTagName("trigeo_link_read")[0]);
      var comment_link_name = GXml.value(channel[0].getElementsByTagName("trigeo_link_comment")[0]);
      
      
    
      
      for (var i = 0; i < markers.length; i++) {
      // obtain the attribues of each marker
      
      
      var coord = GXml.value(markers[i].getElementsByTagName("coordinate")[0]);
      var nid = GXml.value(markers[i].getElementsByTagName("nid")[0]);
      
      
      
      
      var title = GXml.value(markers[i].getElementsByTagName("title")[0]);
      var file_plt = GXml.value(markers[i].getElementsByTagName("file_plt")[0]);
      var geotype = GXml.value(markers[i].getElementsByTagName("geotype")[0]);
      var plt_coord_dim = GXml.value(markers[i].getElementsByTagName("plt_coord_dim")[0]);
      var has_child = GXml.value(markers[i].getElementsByTagName("has_child")[0]);
      var plt_invert = GXml.value(markers[i].getElementsByTagName("plt_invert")[0]);
      var icona = GXml.value(markers[i].getElementsByTagName("icona")[0]);  
      
      //preparazione teaser/////////////////////////////////////////////
      var html = null,thumb = '',comments = '';
      var title = GXml.value(markers[i].getElementsByTagName("title")[0]);
      var teaser = GXml.value(markers[i].getElementsByTagName("teaser")[0]);
      var read_more_link = GXml.value(leggi[i].getElementsByTagName("link")[0]);
      comments += GXml.value(leggi[i].getElementsByTagName("comments")[0]);
      var thumb_path = GXml.value(leggi[i].getElementsByTagName("thumb_path")[0]);
      
      title = '<div id="trigeo_title">' + title + '</div>';
      
      if(thumb_path!='empty')
        thumb = '<div id="trigeo_thumb"><img src="' + thumb_path + '" /></div>';
      
      teaser = '<div id="trigeo_teaser">' + teaser + '</div>';
      
      if(comments != '')
        comments = '<a href="' + comments + '">' + comment_link_name + '</a>';
      
      read_more_link = '<a href="' + read_more_link + '">' + read_link_name + '</a></div>';
      
      html = title +' ' + thumb +' ' + teaser + ' <div id="trigeo_links">' + comments + ' ' + read_more_link + '</div>';
      
      html = '<div id="trigeo_cloud">'+ html + '</div>';
      //preparazione teaser/////////////////////////////////////////////
      
      
      var temp = coord.split(","); 
      var lat = parseFloat(temp[0]);
      var lng = parseFloat(temp[1]);
      var point = new GLatLng(lat,lng);
      
      //var html = 'nome nodo =' + title + ' | nid '+ nid;
      
      
      
      if(geotype=='file .plt' && plt_coord_dim>0)
      {
        var plt = [];

        for(var w=0;w<plt_coord_dim;w++)
        {
          plt[w] = GXml.value(plt_coord[i].getElementsByTagName("geo_"+w)[0])
        }
        
         print_plt2(plt,html,plt_invert,icona);
      }else if(geotype=='punto' && coord!=',')
      {   
        
        cancella = createMarker(point,html,icona);
        map.addOverlay(cancella);
      }
      
      
    
      
      }
    
      
      
      
    }
    }
    request.send(null);
    
    } 
  
  ////////

  
  
  
   
  ////////
   
      
  //funzione per la creazione di un marker
  function createMarker_drag(point) 
  {
     
    
     var xicon = new GIcon(); 
     xicon.iconSize=new GSize(32,32);
          xicon.shadowSize=new GSize(56,32);
          xicon.iconAnchor=new GPoint(16,32);
          xicon.infoWindowAnchor=new GPoint(16,0);
          xicon.image = 'http://maps.google.com/mapfiles/kml/pal2/icon13.png';
          
    var marker = new GMarker(point,{icon:xicon, draggable: true});
    
    
    return marker;
  }
  
    
    
  
  
   function genReq(type,hook) {
    //check if the clean url is enabled
    var out = '';
    var str = document.location;
    
    var clean_url = true;
    str+=' ';
    
    if (str.search("/?q=")==-1)
      clean_url = false;
    
    if(clean_url)
      out = drupal_path + '?q=' + hook + '/' + type + '/';
    else
      out = drupal_path + hook + '/' + type + '/';
    
    return out;
  }
    
   
      
      
       
      
       var map = new GMap2(document.getElementById("trigeo-map-full"));
       map.addControl(new GSmallZoomControl3D() );
       map.addControl(new GMapTypeControl());

       var defZoom = 12; 
   
   
   
       
        
                  
       //poly_nid = document.getElementById("trigeo-poly-nid").value;
       var poly_nid = $("#trigeo-poly-nid").val();
      
       //se bisogna rappresentare una polylinea
       if(poly_nid != null) {
         var pathName = find_xml_path('read-single-element.xml/trigeo/'+poly_nid);
         //alert(pathName);
         var defLat = $("#trigeo-poly-ptm-lat").val();
         var defLon = $("#trigeo-poly-ptm-lon").val();
         
         var pt1 = new GLatLng( $("#trigeo-poly-pt1-lat").val(), $("#trigeo-poly-pt1-lon").val());
         var pt2 = new GLatLng( $("#trigeo-poly-pt2-lat").val(), $("#trigeo-poly-pt2-lon").val());
         
         
         
         var vzoom = 19;
         map.setCenter(new GLatLng( defLat, defLon ), vzoom);
         
         while ( map.getBounds().containsLatLng(pt1) == false || map.getBounds().containsLatLng(pt2) == false ) {
           vzoom--;
           map.setCenter(new GLatLng( defLat, defLon ), vzoom);  
         }
         map.setCenter(new GLatLng( defLat, defLon ), vzoom-1);
         //alert( $("#trigeo-poly-ptm-lon").val() +' '+ $("#trigeo-poly-ptm-lat").val() );
         
         
         marker_from_rssview(pathName,false);
       }
       else {
          //point_nid = document.getElementById("trigeo-point-nid").value;
          
          var Lat = document.getElementById("trigeo-latitude-value").value;
          var Lon = document.getElementById("trigeo-longitude-value").value;
          
          
          map.setCenter(new GLatLng( Lat, Lon ), defZoom);
  
          
          var point = new GLatLng(Lat,Lon);
          marker = createMarker(point,'empty','empty');
          map.addOverlay(marker);
       }
       
        
        



    
        

   
      
      
 
 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  function debug() {
    
  }
  
  function from_routes_to_db(poly) {
      var out = '';
      var dim = poly.getVertexCount();
      //alert('lunghezza' + dim);
      
      
  
      for (var i=1; i<dim-20; i++) {
        var point = '';
        var point = poly.getVertex(i).toString();
        
        point = point.replace(/ /g,"");
        point = point.replace(/\(/g,"");
        point = point.replace(/\)/g,"");
        
        out += point + '_';
      }
      
      
      
      return out;
    }


  
}else { 
  alert("Sorry, the Google Maps API is not compatible with this browser");
} 
