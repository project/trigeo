if (GBrowserIsCompatible()) {

  var cancella;
  var cancella_poly=null;

  function createMarker(point,html,icona) 
    {
    
    var ico = new GIcon();
    if(icona !='empty')
    {
      ico.iconSize = new GSize(40,34);
      ico.iconAnchor = new GPoint(20,34);
      ico.infoWindowAnchor = new GPoint(20,3);
      ico.image = document.location.pathname + icona;
      //ico.image = 'http://localhost/trigeo57/files/images/bici.png';
      //ico.image = 'files/category_pictures/bici.png';
      
    }else
      ico=null;
      
    var marker = new GMarker(point,ico);
      
    GEvent.addListener(marker, "click", function() {
      marker.openInfoWindowHtml(html);
    });
    
    return marker;
    }
  
  function print_plt2(plt_coord,html,plt_invert,icona)
  {
    var tratto = [];
    //var rows = [], coordinate = [];
    var count = 0;
    var poly = '';
    
    for(var i=0;i<plt_coord.length;i++)
    {
      
      
      
      var rows = plt_coord[i].split("_");
      for(var j=0;j<rows.length;j++)
      {
        var coord = rows[j].split(",");
        
        var lat = parseFloat(coord[0]);
        var lon = parseFloat(coord[1]);
        
        tratto[count] = new GLatLng(lat ,lon);
        count++;
      }
      
      
        
      
    }
    tratto[0]=tratto[1];
    
    if(plt_invert == 0)
      cancella = createMarker(tratto[0],html,icona);
    else if(plt_invert == 1)
    {
      cancella = createMarker(tratto[tratto.length-1],html,icona);
    }
    
    
    //controllo se lo zoom � ad un livello
    //appropriato 
    
    
    
    map.addOverlay(cancella);
    cancella_poly = new GPolyline(tratto);
    map.addOverlay(cancella_poly);
    
  
  
    
  }
  
  function find_xml_path(nomehook)
  {
    var str = document.location;
    //aggiunto un chr per problemi di casting
    str+=' ';
    if (str.search("/?q=")==-1)
    {
      //calcolo le dim del nome della pagina corrente
      //per rimuoverlo con un substr 
      var split = str.split("/");
      var cut = split[split.length-1];
      var size = cut.length;
      //aggiungo lo spazio vuoto
      size++;
      var limit = str.length - size;
      //alert(str.substr(0,limit));
      
      out =   str.substr(0,limit) + '/' + nomehook;
    }else
    {
      out = document.location.pathname + '?q=' + nomehook;
    }
    
    return out;
  }
  
  //legge da un file XML informazioni per la creazione di una mappa
    function marker_from_rssview(path,is_child) { 
   // Read the data from example.xml
    
    
    var request = GXmlHttp.create();
    request.open("GET", path, true);
    request.onreadystatechange = function() {
    if (request.readyState == 4) {
      var xmlDoc = GXml.parse(request.responseText);
     
      //seleziono <geo>
      var markers = xmlDoc.documentElement.getElementsByTagName("geo");
      //seleziono il tag con il plt
      var plt_coord = xmlDoc.documentElement.getElementsByTagName("plt_coord");
      //seleziono il tag item
      var leggi =  xmlDoc.documentElement.getElementsByTagName("item");
      //seleziono il tag channel
      var channel =  xmlDoc.documentElement.getElementsByTagName("channel");
      
      var read_link_name = GXml.value(channel[0].getElementsByTagName("trigeo_link_read")[0]);
      var comment_link_name = GXml.value(channel[0].getElementsByTagName("trigeo_link_comment")[0]);
      
      
    
      
      for (var i = 0; i < markers.length; i++) {
      // obtain the attribues of each marker
      
      
      var coord = GXml.value(markers[i].getElementsByTagName("coordinate")[0]);
      var nid = GXml.value(markers[i].getElementsByTagName("nid")[0]);
      
      
      
      
      var title = GXml.value(markers[i].getElementsByTagName("title")[0]);
      var file_plt = GXml.value(markers[i].getElementsByTagName("file_plt")[0]);
      var geotype = GXml.value(markers[i].getElementsByTagName("geotype")[0]);
      var plt_coord_dim = GXml.value(markers[i].getElementsByTagName("plt_coord_dim")[0]);
      var has_child = GXml.value(markers[i].getElementsByTagName("has_child")[0]);
      var plt_invert = GXml.value(markers[i].getElementsByTagName("plt_invert")[0]);
      var icona = GXml.value(markers[i].getElementsByTagName("icona")[0]);  
      
      //preparazione teaser/////////////////////////////////////////////
      var html = null,thumb = '',comments = '';
      var title = GXml.value(markers[i].getElementsByTagName("title")[0]);
      var teaser = GXml.value(markers[i].getElementsByTagName("teaser")[0]);
      var read_more_link = GXml.value(leggi[i].getElementsByTagName("link")[0]);
      comments += GXml.value(leggi[i].getElementsByTagName("comments")[0]);
      var thumb_path = GXml.value(leggi[i].getElementsByTagName("thumb_path")[0]);
      
      title = '<div id="trigeo_title">' + title + '</div>';
      
      if(thumb_path!='empty')
        thumb = '<div id="trigeo_thumb"><img src="' + thumb_path + '" /></div>';
      
      teaser = '<div id="trigeo_teaser">' + teaser + '</div>';
      
      if(comments != '')
        comments = '<a href="' + comments + '">' + comment_link_name + '</a>';
      
      read_more_link = '<a href="' + read_more_link + '">' + read_link_name + '</a></div>';
      
      html = title +' ' + thumb +' ' + teaser + ' <div id="trigeo_links">' + comments + ' ' + read_more_link + '</div>';
      
      html = '<div id="trigeo_cloud">'+ html + '</div>';
      //preparazione teaser/////////////////////////////////////////////
      
      
      var temp = coord.split(","); 
      var lat = parseFloat(temp[0]);
      var lng = parseFloat(temp[1]);
      var point = new GLatLng(lat,lng);
      
      //var html = 'nome nodo =' + title + ' | nid '+ nid;
      
      
      
      if(geotype=='file .plt' && plt_coord_dim>0)
      {
        var plt = [];

        for(var w=0;w<plt_coord_dim;w++)
        {
          plt[w] = GXml.value(plt_coord[i].getElementsByTagName("geo_"+w)[0])
        }
        
         print_plt2(plt,html,plt_invert,icona);
      }else if(geotype=='punto' && coord!=',')
      {   
        
        cancella = createMarker(point,html,icona);
        map.addOverlay(cancella);
      }
      
      
    
      
      }
    
      
      
      
    }
    }
    request.send(null);
    
    } 
  
  ////////

  
  
  
   
  ////////
   
      
  //funzione per la creazione di un marker
  function createMarker_drag(point) 
  {
     
    
     var xicon = new GIcon(); 
     xicon.iconSize=new GSize(32,32);
          xicon.shadowSize=new GSize(56,32);
          xicon.iconAnchor=new GPoint(16,32);
          xicon.infoWindowAnchor=new GPoint(16,0);
          xicon.image = 'http://maps.google.com/mapfiles/kml/pal2/icon13.png';
          
    var marker = new GMarker(point,{icon:xicon, draggable: true});
    
    
    return marker;
  }
    
  function centerMarker() {
      marker.setPoint(new GLatLng(map.getCenter().lat(), map.getCenter().lng()))
      document.getElementById('edit-latitude').value = marker.getPoint().lat();
      document.getElementById('edit-longitude').value = marker.getPoint().lng();
  }
  
    function genReq(type,hook) {
    //check if the clean url is enabled
    var out = '';
    var str = document.location;
    var clean_url = true;
    str+=' ';
    
    if (str.search("/?q=")==-1)
      clean_url = false;
    
    if(clean_url)
      out = drupal_path + '?q=' + hook + '/' + type + '/';
    else
      out = drupal_path + hook + '/' + type + '/';
    
    return out;
  }
    
   
        // creo la mappa punto
       var map = new GMap2(document.getElementById("map"));
       map.addControl(new GLargeMapControl3D() );
       map.addControl(new GMapTypeControl());
       
      

        
       var defLat = 42.992092276402694;
       var defLon = 12.495231628417969;
       var defZoom = 7;
       
           
                  
        var Lat = document.getElementById("edit-latitude").value;
        var Lon = document.getElementById("edit-longitude").value;
        
        if((Lat=='')&&(Lon==''))
        {
          //se sono prensenti cordinate carico il marker sulla posizione
          //memorizzata
          map.setCenter(new GLatLng( defLat, defLon ), defZoom);
          var point = new GLatLng(43,12);
          marker = createMarker_drag(point);
          map.addOverlay(marker);
          
          
          document.getElementById("edit-latitude").value= defLat;
          document.getElementById("edit-longitude").value= defLon;
        }else
        {
          map.setCenter(new GLatLng( Lat, Lon ), defZoom);
          //altrimenti lo carico nelle cordinate di default
          var point = new GLatLng(Lat,Lon);
          marker = createMarker_drag(point);
          map.addOverlay(marker);
        }
      
        GEvent.addListener(marker, "dragend", function() {
          
          var latlon = ' ' 
          latlon += marker.getLatLng();
      
          var sp = latlon.split(',');
          
          var lat = sp[0].substr(2,sp[0].length);
          var lon = sp[1].substr(1,sp[1].length-2);
          
          
          document.getElementById("edit-latitude").value = lat;
          document.getElementById("edit-longitude").value = lon;
              
        })

      
        
        $(document).ready(function(){ 
            
            $("#edit-parent").ready(function()
            {
              var pathName;
              
              pathName = genReq('trigeo','read-single-element.xml') + $("#edit-parent").val();
              
              $("#out").empty(); 
              //debug print value
              //$("#out").append(pathName);
            
            marker_from_rssview(pathName,false);
            
            });
            
            $("#edit-parent").change(function() 
            { 
                
                if(cancella_poly!=null)
                  map.removeOverlay(cancella_poly); 
                
                map.removeOverlay(cancella);
                
                var pathName;
              
              pathName = genReq('trigeo','read-single-element.xml') + $("#edit-parent").val();
              
              $("#out").empty(); 
              //debug print value
              //$("#out").append(pathName);
            
            marker_from_rssview(pathName,false);
            
            });
            
            
            
            
        });
 
 
            
 
 
  ///////////////////////////////////////////////////       ROUTES     /////////////////////////////////////////////////////
 
 
 //creo la mappa routes 
      var map2 = new GMap(document.getElementById("map2"));
      map2.addControl(new GLargeMapControl3D());
      map2.addControl(new GMapTypeControl());
      map2.setCenter(new GLatLng(defLat,defLon),12);
      var bounds = new GLatLngBounds();
 
 
  
      // ====== Create a Client Geocoder ======
      var geo = new GClientGeocoder(new GGeocodeCache()); 

      // ====== Array for decoding the failure codes ======
      var reasons=[];
      reasons[G_GEO_SUCCESS]            = "Success";
      reasons[G_GEO_MISSING_ADDRESS]    = "Missing Address: The address was either missing or had no value.";
      reasons[G_GEO_UNKNOWN_ADDRESS]    = "Unknown Address:  No corresponding geographic location could be found for the specified address.";
      reasons[G_GEO_UNAVAILABLE_ADDRESS]= "Unavailable Address:  The geocode for the given address cannot be returned due to legal or contractual reasons.";
      reasons[G_GEO_BAD_KEY]            = "Bad Key: The API key is either invalid or does not match the domain for which it was given";
      reasons[G_GEO_TOO_MANY_QUERIES]   = "Too Many Queries: The daily geocoding quota for this site has been exceeded.";
      reasons[G_GEO_SERVER_ERROR]       = "Server error: The geocoding request could not be successfully processed.";
      reasons[G_GEO_BAD_REQUEST]        = "A directions request could not be successfully parsed.";
      reasons[G_GEO_MISSING_QUERY]      = "No query was specified in the input.";
      reasons[G_GEO_UNKNOWN_DIRECTIONS] = "The GDirections object could not compute directions between the points.";
      
      // ====== Geocoding ======
      
      
      
      function showAddress(mapname) {
        
        if(mapname == 'map2'){
            if (state==0) {
              var search = document.getElementById("edit-start").value;
              addresses[0] = search;
              
              //numero delle fermate intermedie
              stops += parseInt($("#stops").val());
              for (var y=0; y<stops ;y++) {
        
               if(y==0) { 
                 active[y] = true;
               }
               else {
                 active[y] = false;
               }
               
              }
              active[stops]=true;
              $("#stops").attr("disabled","disabled");
              //imposto il numero delle fermate intermedie
              
            }
            if (state==1) {
              var search = document.getElementById("edit-end").value;
              addresses[4] = search;
            }
            geo.getLatLng(search, function (point)
              { 
                if (point) {
                  
                  if (state==1) {
                    doEnd(point);
                    map2.setCenter(point,14); 
                  }
                  if (state==0) {
                    doStart(point);
                    map2.setCenter(point,14);
                  }
                  
                  
                }
                else {
                  var result=geo.getCache().get(search);
                  if (result) {
                    var reason="Code "+result.Status.code;
                    if (reasons[result.Status.code]) {
                      reason = reasons[result.Status.code]
                    }
                  } else {
                    var reason = "";
                  } 
                  alert('Could not find "'+search+ '" ' + reason);
                }
              }
            );
            
            
            
            
        }
        else if(mapname == 'map'){
           
           var search = document.getElementById("edit-address").value;
           geo.getLocations(search, function (result) { 
              // If that was successful
              if (result.Status.code == G_GEO_SUCCESS) 
              {
             
                var p = result.Placemark[0].Point.coordinates;
                map.setCenter(new GLatLng(p[1],p[0]),14);
                centerMarker();
              }
              // ====== Decode the error status ======
              else 
              {
                var reason="Code "+result.Status.code;
                if (reasons[result.Status.code]) 
                {
                  reason = reasons[result.Status.code]
                } 
                alert('Could not find "'+search+ '" ' + reason);
              }
           });
        }
        
        
      }

      function swapMarkers(i) {
        map2.removeOverlay(gmarkers[i]);
        createMarker2(path[i],i,icon2);
      }

      var baseIcon = new GIcon(G_DEFAULT_ICON);
      baseIcon.iconSize=new GSize(24,38);

      var icon1 = G_START_ICON;
      var icon2 = G_PAUSE_ICON;
      var icon3 = G_END_ICON;
      var icon4 = new GIcon(baseIcon,"http://labs.google.com/ridefinder/images/mm_20_white.png");
          icon4.shadow = "http://labs.google.com/ridefinder/images/mm_20_shadow.png";
          icon4.iconSize = new GSize(12, 20);
          icon4.shadowSize = new GSize(22, 20);
          icon4.iconAnchor = new GPoint(6, 20);
          icon4.infoWindowAnchor = new GPoint(5, 1);


      function createMarker2(point,i,icon) {
        var marker = new GMarker(point, {draggable:true,icon:icon});
        gmarkers[i]=marker;
        GEvent.addListener(marker, "dragend", function() {
          path[i] = marker.getPoint();
          if (!active[i]) {
            setTimeout('swapMarkers('+i+')', 1000);
          }
          active[i] = true;
          addresses[i] = "";
        });
        map2.addOverlay(marker);
      }
      

      // ===== Array to contain the points of the path =====
      
      
      var stops = 1;
      var active = [];
      
     /* var stops = 4;

      
      var active = [];
      for (var y=0; y<stops ;y++) {
      
       if(y==0) { 
         active[y] = true;
       }
       else {
         active[y] = false;
       }
       
      }
      active[stops]=true;*/
      
      var path = [];
      //var active = [true,false,false,false,true];
      var gmarkers = [];
      var addresses = [];

      // ===== State Driven Processing =====
      var state = 0;

      function handleState() {
        if (state == 0) {
          document.getElementById("edit-start-wrapper").style.display = "block";
          document.getElementById("prefix-start").style.display = "block";
          document.getElementById("edit-end-wrapper").style.display = "none";
          document.getElementById("prefix-end").style.display = "none";
          //document.getElementById("drag").style.display = "none";
        }
        if (state == 1) {
          document.getElementById("edit-start-wrapper").style.display = "none";
          document.getElementById("prefix-start").style.display = "none";
          document.getElementById("edit-end-wrapper").style.display = "block";
          document.getElementById("prefix-end").style.display = "block";
          //document.getElementById("drag").style.display = "none";
        }
        if (state == 2) {
          document.getElementById("edit-start-wrapper").style.display = "none";
          document.getElementById("prefix-start").style.display = "none";
          document.getElementById("edit-end-wrapper").style.display = "none";
          document.getElementById("prefix-end").style.display = "none";
          //document.getElementById("drag").style.display = "block";
        }                                                       
      }

      handleState();

      GEvent.addListener(map2, "click", function(overlay,point) {
        if (point) {
          if (state == 1) { doEnd(point) }
          if (state == 0) { 
            
            //numero delle fermate intermedie
            stops += parseInt($("#stops").val());
            for (var y=0; y<stops ;y++) {
      
             if(y==0) { 
               active[y] = true;
             }
             else {
               active[y] = false;
             }
             
            }
            active[stops]=true;
            $("#stops").attr("disabled","disabled");
            //imposto il numero delle fermate intermedie
            
            doStart(point);
          }
        }
      });

      function doStart(point) {
        createMarker2(point,0,icon1);
        path[0] = point;
        state = 1;
        handleState();
      }

      function doEnd(point) {
        stops
        createMarker2(point,stops,icon3);
        path[stops] = point;
        state = 2;
        handleState();
        for (var i=1; i<stops; i++) {
          var lat = (path[0].lat()*(stops-i) + path[stops].lat()*i)/stops;
          var lng = (path[0].lng()*(stops-i) + path[stops].lng()*i)/stops;
          var p = new GLatLng(lat,lng);
          createMarker2(p,i,icon4);
          path[i] = p;
        }
        bounds.extend(path[0]);
        bounds.extend(path[stops]);
        map2.setZoom(map2.getBoundsZoomLevel(bounds));
        map2.setCenter(bounds.getCenter());
      }


      var gdir=new GDirections(null, document.getElementById("path"));

      GEvent.addListener(gdir,"error", function() {
        var code = gdir.getStatus().code;
        var reason="Code "+code;
        if (reasons[code]) {
          reason = "Code "+code +" : "+reasons[code]
        } 
        alert("Failed to obtain directions, "+reason);
      });

      var poly;
      GEvent.addListener(gdir, "load", function() {
        if (poly) map2.removeOverlay(poly);
        poly = gdir.getPolyline();
        map2.addOverlay(poly);
        
        //conversione ed inserimento tramite dom dei punti del percorso nella form di drupal
        document.getElementById('edit-route-points').value = from_routes_to_db(poly);
        
      });
        

      function directions() {
        
        if (addresses[0]) {var a = addresses[0] + "@" + path[0].toUrlValue(6)}
          else {var a = path[0].toUrlValue(6)} 
        if (addresses[stops]) {var b = addresses[stops] + "@" + path[stops].toUrlValue(6)}
          else {var b = path[stops].toUrlValue(6)} 
        for (var i=stops-1; i>0; i--) {
          if (active[i]) {
            b = path[i].toUrlValue(6) +" to: "+b;
          }
        }
        var a = "from: "+a + " to: " + b;
        
        
        
        if( $("#edit-route-language").val()!=null ) {
          var route_lang = $("#edit-route-language").val();
        }
        else {
          var route_lang = 'en';
        }
        
        
        gdir.load(a, {getPolyline:true,"locale":route_lang});
        $("#copyroute").css("display","inline");
          
      }
      
      function copy() {
        var text = $("#path").html();
        var add = $("#edit-body").val() + '\n\n' + text ; 
        $("#edit-body").val(add);
        
        $("#copyroute").attr("disabled","disabled");
      }
      
 
 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  function debug() {
    
  }
  
  function from_routes_to_db(poly) {
      var out = '';
      var dim = poly.getVertexCount();
      //alert('lunghezza' + dim);
      
      
  
      for (var i=1; i<dim-20; i++) {
        var point = '';
        var point = poly.getVertex(i).toString();
        
        point = point.replace(/ /g,"");
        point = point.replace(/\(/g,"");
        point = point.replace(/\)/g,"");
        
        out += point + '_';
      }
      
      
      
      return out;
    }
  


  
  //USER INTERFACE JQUERY ELEMENTS LOAD
  $("#copyroute").css("display","none");
  $("#edit-plt-invert-wrapper").css("display","none");
  $("#map2").css("display","none");
  $("#edit-start-wrapper").css("display","none");
  $("#prefix-start").css("display","none");
  $("#prefix-plt-invert").css("display","none");
  $("#drag").css("display","none");
  $("#edit-route-points-wrapper").css("display","none");
  
  
  
  $("#edit-geotype").change(function() {
    
    var value = $("#edit-geotype").val();
    
    if( value == 'punto') {
      $("#edit-plt-invert-wrapper").css("display","none");
      $("#edit-plt-invert-wrapper").css("display","none");
      $("#drag").css("display","none");
      $("#map2").css("display","none");
      $("#prefix-start").css("display","none");
      $("#edit-start-wrapper").css("display","none");
      $("#prefix-plt-invert").css("display","none");
      $("#edit-end-wrapper").css("display","none");
      
      
      $("#edit-latitude-wrapper").fadeIn("slow");
      $("#edit-longitude-wrapper").fadeIn("slow");
      $("#map").fadeIn("slow");
      $("#retrivemarker").fadeIn("slow");
      $(".map-description").fadeIn("slow");
      $("#edit-address-wrapper").fadeIn("slow");
      $(".prefix-address").fadeIn("slow");
      
    }
    else if( value == 'file .plt') {
      $("#edit-latitude-wrapper").css("display","none");
      $("#edit-longitude-wrapper").css("display","none");
      $("#map").css("display","none");
      $("#retrivemarker").css("display","none");
      $(".map-point-desc").css("display","none");
      $("#map2").css("display","none");
      $("#edit-address-wrapper").css("display","none");
      $(".prefix-address").css("display","none");
      $("#prefix-start").css("display","none");
      $("#edit-start-wrapper").css("display","none");
      $("#drag").css("display","none");
      $("#edit-end-wrapper").css("display","none");
      
      
      $("#edit-plt-invert-wrapper").fadeIn("slow");
      $("#prefix-plt-invert").fadeIn("slow");
    }
    else if( value == 'routes') {
      $("#edit-plt-invert-wrapper").css("display","none");
      $("#edit-latitude-wrapper").css("display","none");
      $("#edit-longitude-wrapper").css("display","none");
      $(".map-point-desc").css("display","none");
      $("#retrivemarker").css("display","none");
      $("#map").css("display","none");
      $("#edit-address-wrapper").css("display","none");
      $(".prefix-address").css("display","none");
      $("#prefix-plt-invert").css("display","none");
      
      
      $("#drag").fadeIn("slow");
      $("#map2").fadeIn("slow");
      //se non � stato inserito alcun dato sul percorso
      if(state == 0) {
        $("#edit-start-wrapper").fadeIn("slow");
        $("#prefix-start").fadeIn("slow");
      }
      else if(state == 1) {
        $("#edit-end-wrapper").fadeIn("slow");
      }
      
    }
    
    
  });

  
}else { 
  alert("Sorry, the Google Maps API is not compatible with this browser");
} 
