if (GBrowserIsCompatible()) {
    
    
    
    function checkBox(nid)
    {
        for(var p=0;p<memory.length;p++)
        {
          if(nid==memory[p].nid)
          {
            if(memory[p].type=='punto')
            {
              if(memory[p].obj.isHidden())
              { 
                memory[p].obj.show();
                memory[p].show=true;
              }
              else
              {
                memory[p].obj.hide();
                memory[p].show=false;
              }
              
            }else if(memory[p].type=='file .plt')
            {
              if(memory[p].obj.marker.isHidden())
              { 
                memory[p].obj.marker.show();
                memory[p].obj.poly.show();
                memory[p].show=true;
              }
              else
              {
                memory[p].obj.marker.hide();
                memory[p].obj.poly.hide();
                memory[p].show=false;
              }
            }
          }
        }
        
        
    }
    
    function checkboxRef()
    {
        for(var p=0;p<memory.length;p++)
        {
            if(memory[p].type=='punto')
            {
              memory[p].obj.show();
            }
            else if(memory[p].type=='file .plt')
            {
              memory[p].obj.poly.show();
              memory[p].obj.marker.show();
            }
        }
    }
    
    
    
    
    function poly_reduce(poly,approx)
    {
      var tratto = [];
      poly.getVertex(tratto);
      var short = [];
      var q=1;
      short[short.length]=tratto[0];
      for(p=0;p<tratto.length;p++)
      {
        if(q==approx)
        { 
          short[short.length] = tratto[p];
          q=1;
        }else
          q++;
      }
      short[short.length]=tratto[tratto.length-1];
      
      return short;
    }

    // A function to create the marker and set up the event window
    function createMarker(point,html,icona) 
    {
     
    var ico = new GIcon();
    if(icona !='empty')
    {
      ico.iconSize = new GSize(40,34);
      ico.iconAnchor = new GPoint(20,34);
      ico.infoWindowAnchor = new GPoint(20,3);
      //ico.image = document.location.pathname + icona;
      ico.image = find_xml_path(icona,true);
      //ico.image = 'http://localhost/trigeo57/files/images/bici.png';
      //ico.image = 'files/category_pictures/bici.png';
      
    }else
      ico=null;
      
    var marker = new GMarker(point,ico);
      
    GEvent.addListener(marker, "click", function() {
      marker.openInfoWindowHtml(html);
    });
    
    return marker;
    }
    
    


    // funzione che associa il percorso e il marker al checkbox
    function myclick(i) {
    
    if(onoff[i]==1){
      map.removeOverlay(kml[i]);
      
      if(kml_str[i])
      map.removeOverlay(kml_str[i]);
      
      onoff[i]=0;
    }
    else{
      map.addOverlay(kml[i]);
      
      if(kml_str[i]){
      map.addOverlay(kml_str[i]);
      GEvent.trigger(kml_str[i], "click");
      }
      
      onoff[i]=1;
    }
    }
    
    //f(x) per "pulire" i nomi dei file del GPS
    //visualizzati sulla barra di navigazione
    function clear_name(word,s,e) {
      
    var brick = word.split(".");

    var tmp;  
    
    word=null;
    
    for(var i=0;i<=brick.length-2;i++)
        word = word + brick[i];
    
    //alert(word);
    tmp=word.substr(s+3,e);
      //alert(tmp);
      
      word=tmp.replace(/_/g,' ');
    

    brick = word.split(" ");
    word = '';
    for( i=0;i < brick.length; i++ ) {
      
      
      if(IsNumeric(brick[i]) == false)
        word = word + ' ' + brick[i];
      
    } 
      return word;
    }
    //controlla se una striga è un numero
    function IsNumeric(sText) {
    
     var ValidChars = "0123456789.";
     var IsNumber=true;
     var Char;
  
   
     for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
       {
       IsNumber = false;
       }
      }
     return IsNumber;
     
    }
    
   
   
   
   
  //direttamente da un file plt tramite $ esegue
  //la lettura di un file e lo converte in polyline
  //visualizzabile su mappa
  function plt_to_poly(file,html)
  {
    $.get(file,function(data)
    {
        //alert("Data Loaded: " + data);       
       var punti = [];
       var p=0;
       var markerLon = null; 
       var markerLat = null;
        
        stampa = '';
   
        
        riga = data.split("\n");
        max = riga.length-2;
        
        //trovo la prima riga con le coordinate
        var k=0;
        var check = riga[k].split(",");
        while(check.length!=7)
        {
          k++;
          check = riga[k].split(",");
        }   
        
        //eseguo il parsing e acquisico solo le coordinate distinte
        //fino a quando trovo righe da leggere
        var i = k;
        check = riga[i].split(",");
        
        
        while(check.length==7)
        {
          i++;
          check = riga[i].split(",");
          
          
          if(check.length == 7)
          {           
            
            colonna = riga[i].split(",");
            
            colonnaPrec=null;
            
            if(i>6)
              colonnaPrec = riga[i-1].split(",");
            else if(i==6)
              colonnaPrec = true;
            else
              colonnaPrec = false
            
            if(colonnaPrec!=false)
            {
              if( colonna[0]!=colonnaPrec[0] && colonna[1]!=colonnaPrec[1] )
              {
                
                stampa+=colonna[0]+',';
                stampa+=colonna[1];
                stampa+='_';
                
               
               punti[p] = new GLatLng(colonna[0],colonna[1]);
               p++;
               
               //prendo il secondo punto della polyline per creare il markers
               if(!markerLon && !markerLat && p==1 )
                {
                 markerLon = check[0];
                 markerLat = check[1];
                }           
              }
            }
          }
          
        
        }
      
      //alert(stampa); 
      //debug alert(markerLon +' '+ markerLat);
      var point = new GLatLng(markerLon,markerLat);     
      test = createMarker(point,html);
      map.addOverlay(test);
       
         //creo il percorso con gmaps libs      
          map.addOverlay(new GPolyline(punti));
  
        document.getElementById("main").innerHTML = stampa;
   
    }
    );
  }



   
   //legge da un file XML informazioni per la creazione di una mappa
    function marker_from_rssview(path,is_child,quadr) { 
   // Read the data from example.xml
    var request = GXmlHttp.create();
    request.open("GET", path, true);
    request.onreadystatechange = function() {
    if (request.readyState == 4) {
      var xmlDoc = GXml.parse(request.responseText);
      
      
      
      //seleziono <geo>
      var markers = xmlDoc.documentElement.getElementsByTagName("geo");
      //seleziono il tag con il plt
      var plt_coord = xmlDoc.documentElement.getElementsByTagName("plt_coord");
      //seleziono il tag item
      var leggi =  xmlDoc.documentElement.getElementsByTagName("item");
      //seleziono il tag channel
      var channel =  xmlDoc.documentElement.getElementsByTagName("channel");
      
      var read_link_name = GXml.value(channel[0].getElementsByTagName("trigeo_link_read")[0]);
      var comment_link_name = GXml.value(channel[0].getElementsByTagName("trigeo_link_comment")[0]);
      
      
      //azzero present
      var present = [];
     
      
      for (var i = 0; i < markers.length; i++) {
      // obtain the attribues of each marker
      
      
      var coord = GXml.value(markers[i].getElementsByTagName("coordinate")[0]);
      var nid = GXml.value(markers[i].getElementsByTagName("nid")[0]);
      

      var title = GXml.value(markers[i].getElementsByTagName("title")[0]);
      var title_clean = GXml.value(markers[i].getElementsByTagName("title")[0]);
      var file_plt = GXml.value(markers[i].getElementsByTagName("file_plt")[0]);
      var geotype = GXml.value(markers[i].getElementsByTagName("geotype")[0]);
      var plt_coord_dim = GXml.value(markers[i].getElementsByTagName("plt_coord_dim")[0]);
      var has_child = GXml.value(markers[i].getElementsByTagName("has_child")[0]);
      var plt_invert = GXml.value(markers[i].getElementsByTagName("plt_invert")[0]);
      var icona = GXml.value(markers[i].getElementsByTagName("icona")[0]);  
      //preparazione teaser/////////////////////////////////////////////
      var html = null,thumb = '',comments = '';
      var title = GXml.value(markers[i].getElementsByTagName("title")[0]);
      var teaser = GXml.value(markers[i].getElementsByTagName("teaser")[0]);
      var read_more_link = GXml.value(leggi[i].getElementsByTagName("link")[0]);
      comments += GXml.value(leggi[i].getElementsByTagName("comments")[0]);
      var thumb_path = GXml.value(leggi[i].getElementsByTagName("thumb_path")[0]);
      
      title = '<div id="trigeo_title">' + title + '</div>';
      
      if(thumb_path!='empty')
        thumb = '<div id="trigeo_thumb"><img src="' + thumb_path + '" /></div>';
      
      teaser = '<div id="trigeo_teaser">' + teaser + '</div>';
      
      if(comments != '')
        comments = '<a href="' + comments + '">' + comment_link_name + '</a>';
      
      read_more_link = '<a href="' + read_more_link + '">' + read_link_name + '</a></div>';
      
      html = title +' ' + thumb +' ' + teaser + ' <div id="trigeo_links">' + comments + '' + read_more_link + '</div>';
      
      html = '<div id="trigeo_cloud">'+ html + '</div>';
      //preparazione teaser/////////////////////////////////////////////
      
      
      var temp = coord.split(","); 
      var lat = parseFloat(temp[0]);
      var lng = parseFloat(temp[1]);
      var point = new GLatLng(lat,lng);
      //var html = 'nome nodo =' + title + ' | nid '+ nid;
      
      
      present[i]=nid;
      
      var load = true;
      for(var q=0;q<past.length;q++)
      {
        if(nid==past[q])
          load=false;
      }
      
      if(load)
      {
        var obj = '';
        memory[memory.length] = new data;
      
        
        if(geotype=='file .plt' && plt_coord_dim>0)
        {
          var plt = [];
  
          for(var w=0;w<plt_coord_dim;w++)
          {
            plt[w] = GXml.value(plt_coord[i].getElementsByTagName("geo_"+w)[0])
          }
          
          obj = print_plt2(plt,html,plt_invert,icona);
          memory[memory.length-1].type = 'file .plt';
          
        }else if(geotype=='punto' && coord!=',')
        {   
          
          marker = createMarker(point,html,icona);
          //map.addOverlay(marker);
          
          obj = marker;
          memory[memory.length-1].type = 'punto';
        }
  
        
        memory[memory.length-1].nid = nid;
        memory[memory.length-1].obj = obj;
        memory[memory.length-1].has_child = has_child;
        memory[memory.length-1].title = title_clean;
        
      }
        
        
        

          

      
    
  
    }
    
    
    var add = [];
    var rem = [];
    var flag;
    
    
    
    for(var q=0;q<past.length;q++)
    {
      flag = true;
      for(var p=0;p<present.length;p++)
      {
        if(past[q]==present[p])
          flag = false;
      }
      
      if(flag)
        rem[rem.length]=past[q];
      
    }
    
    
    for(var q=0;q<present.length;q++)
    {
      flag = true;
      for(var p=0;p<past.length;p++)
      {
        if(present[q]==past[p])
          flag = false;
      }
      
      if(flag)
        add[add.length]=present[q];
    }
    
    
    past = present;
    
    
    
    var zoom = map.getZoom();
    
    
    
    //aggiungo elementi
    for(var q=0;q<add.length;q++)
    {
      for(var p=0;p<memory.length;p++)
      {
        if(add[q]==memory[p].nid)
        {
            if(memory[p].type=='punto')
            {
              map.addOverlay(memory[p].obj);
              
              if(memory[p].has_child==1)
                marker_from_rssview_child( find_xml_path('read-child.xml/x/x/' + memory[p].nid,false),memory[p],'load');
              
              
                
            }else if(memory[p].type=='file .plt')
            {
              
              
          
              //integrazione con il livello di dettaglio dello zoom
          
                  
                if(pltRedFact[zoom]!=0)
                {
                  
                  
                  if(memory[p].poly_bk!=null)
                  {
                    temp = reduce_plt(memory[p].poly_bk,pltRedFact[zoom]);
                    memory[p].obj.poly = temp;
                  }else
                  {
                    memory[p].poly_bk = memory[p].obj.poly;
                    temp = reduce_plt(memory[p].obj.poly,pltRedFact[zoom]);
                    memory[p].obj.poly = temp;
                  }
                  
                  
                  map.addOverlay(memory[p].obj.poly);
                    
                    if(memory[p].has_child==1)
                    marker_from_rssview_child( find_xml_path('read-child.xml/x/x/' + memory[p].nid,false),memory[p],'load');
                  
                }
              
              
              
              //integrazione con il livello di dettaglio dello zoom
              
              map.addOverlay(memory[p].obj.marker);

            }
        }
      }
    }
    
    //rimuovo elementi
    for(var q=0;q<rem.length;q++)
    {
      for(var p=0;p<memory.length;p++)
      {
        if(rem[q]==memory[p].nid)
        {
    
            if(memory[p].type=='punto')
            {
              map.removeOverlay(memory[p].obj);
              
              if(memory[p].has_child==1)
                marker_from_rssview_child( ' ' ,memory[p],'remove');
                
              memory.splice(p,1);
              
              
              
            }else if(memory[p].type=='file .plt')
            {
              map.removeOverlay(memory[p].obj.poly);
              map.removeOverlay(memory[p].obj.marker);
              
              if(memory[p].has_child==1)
                marker_from_rssview_child( ' ' ,memory[p],'remove');
                
              
              memory.splice(p,1);
            }
        }
      }
    }
    
    
    
    
    
    if(zoom!=zoomPrev)
    {   
      for(var p=0;p<memory.length;p++)
      {     
        if(memory[p].type=='file .plt' && (pltRedFact[zoom]==0) )
        {
          map.removeOverlay(memory[p].obj.poly);
          
          if(memory[p].has_child==1)
            marker_from_rssview_child( ' ' ,memory[p],'remove');
            
        }else if(memory[p].type=='file .plt' && (pltRedFact[zoom]!=0) )
        {
          
          map.removeOverlay(memory[p].obj.poly);
          if(memory[p].has_child==1)
            marker_from_rssview_child( ' ' ,memory[p],'remove');
          
          if(memory[p].poly_bk!=null)
          {
            temp = reduce_plt(memory[p].poly_bk,pltRedFact[zoom]);
            memory[p].obj.poly = temp;
          }else
          {
            memory[p].poly_bk = memory[p].obj.poly;
            temp = reduce_plt(memory[p].obj.poly,pltRedFact[zoom]);
            memory[p].obj.poly = temp;
          }
          
          if(memory[p].has_child==1)
            marker_from_rssview_child( find_xml_path('read-child.xml/x/x/' + memory[p].nid,false),memory[p],'load');
          
          map.addOverlay(memory[p].obj.poly);
          
        }
      }
    }
    
    zoomPrev=zoom;
    
    
    
    
      
    }
    }
    request.send(null);
    
    }
  
  
  
  
  
   function marker_from_rssview_child(path,father,action) { 
    
    if(action == 'load')
    {
        
        var request = GXmlHttp.create();
        request.open("GET", path, true);
        request.onreadystatechange = function() {
        if (request.readyState == 4) {
          var xmlDoc = GXml.parse(request.responseText);
          //seleziono <geo>
          var markers = xmlDoc.documentElement.getElementsByTagName("geo");
          //seleziono il tag con il plt
          var plt_coord = xmlDoc.documentElement.getElementsByTagName("plt_coord");
          //seleziono il tag item
          var leggi =  xmlDoc.documentElement.getElementsByTagName("item");
          //seleziono il tag channel
          var channel =  xmlDoc.documentElement.getElementsByTagName("channel");
          var read_link_name = GXml.value(channel[0].getElementsByTagName("trigeo_link_read")[0]);
          var comment_link_name = GXml.value(channel[0].getElementsByTagName("trigeo_link_comment")[0]);
          
          for (var i = 0; i < markers.length; i++) {
          // obtain the attribues of each marker
          
          var coord = GXml.value(markers[i].getElementsByTagName("coordinate")[0]);
          var nid = GXml.value(markers[i].getElementsByTagName("nid")[0]);
    
          var title = GXml.value(markers[i].getElementsByTagName("title")[0]);
          var file_plt = GXml.value(markers[i].getElementsByTagName("file_plt")[0]);
          var geotype = GXml.value(markers[i].getElementsByTagName("geotype")[0]);
          var plt_coord_dim = GXml.value(markers[i].getElementsByTagName("plt_coord_dim")[0]);
          var has_child = GXml.value(markers[i].getElementsByTagName("has_child")[0]);
          var plt_invert = GXml.value(markers[i].getElementsByTagName("plt_invert")[0]);
          var icona = GXml.value(markers[i].getElementsByTagName("icona")[0]);  
          //preparazione teaser/////////////////////////////////////////////
          var html = null,thumb = '',comments = '';
          var title = GXml.value(markers[i].getElementsByTagName("title")[0]);
          var teaser = GXml.value(markers[i].getElementsByTagName("teaser")[0]);
          var read_more_link = GXml.value(leggi[i].getElementsByTagName("link")[0]);
          comments += GXml.value(leggi[i].getElementsByTagName("comments")[0]);
          var thumb_path = GXml.value(leggi[i].getElementsByTagName("thumb_path")[0]);
          
          title = '<div id="trigeo_title">' + title + '</div>';
          
          if(thumb_path!='empty')
            thumb = '<div id="trigeo_thumb"><img src="' + thumb_path + '" /></div>';
          
          teaser = '<div id="trigeo_teaser">' + teaser + '</div>';
          
          if(comments != '')
            comments = '<a href="' + comments + '">' + comment_link_name + '</a>';
          
          read_more_link = '<a href="' + read_more_link + '">' + read_link_name + '</a></div>';
          
          html = title +' ' + thumb +' ' + teaser + ' <div id="trigeo_links">' + comments + ' ' + read_more_link + '</div>';
          
          html = '<div id="trigeo_cloud">'+ html + '</div>';
          //preparazione teaser/////////////////////////////////////////////
          
          
          var temp = coord.split(","); 
          var lat = parseFloat(temp[0]);
          var lng = parseFloat(temp[1]);
          var point = new GLatLng(lat,lng);
          
    
          if(geotype=='punto' && coord!=',')
          {   
            
            marker = createMarker(point,html,icona);
            map.addOverlay(marker);
            
            father.childs[father.childs.length] = marker;
          }
          
      
        }

      
        }
        }
        request.send(null);
        
            
        
    }else if(action == 'remove')
    {
      for(var i=0;i<father.childs.length;i++)
        map.removeOverlay(father.childs[i]);
    }
    
    
   }

  
  
  function create_geoList()
  {
    //parte che crea il menu con checkbox relativo
    //ai elmenti correnti
    var sideBarLeft = '';
    
    for(var p=0;p<list.length;p++)
    {
      var stato = '';
      if(list[p].show==true)
        stato='checked="checked"';
      
      sideBarLeft += '<div class="geoSel"><input type="checkbox" onClick="javascript:checkBox(' + list[p].nid + ')" '+ stato +' />'+ list[p].title +'</div>';
      
      
    }
    
    document.getElementById("menucheck").innerHTML = sideBarLeft;
  }
  
    
    
  
    
  function find_xml_path(nomehook,isIcon)
  {
    var meno = 0;
    
    
    var str = document.location;
    //aggiunto un chr per problemi di casting
    str+=' ';
    if (str.search("/?q=")==-1)
    {
      //calcolo le dim del nome della pagina corrente
      //per rimuoverlo con un substr 
      var split = str.split("/");
      var cut = split[split.length-1];
      var size = cut.length;
      //aggiungo lo spazio vuoto
      size++;
      var limit = str.length - size;
      //alert(str.substr(0,limit));
      
      out =   str.substr(0,limit) + '/' + nomehook;
    }else
    {
      out = document.location.pathname + '?q=' + nomehook;
    }
    
    if(  typeof(EST) != 'undefined' )
    {
      var mod = out.split("/");
      out = mod[1] + '/' + mod[mod.length-1];
    }
    
    if(isIcon==true)
    {
      var temp = out.split("?q=");
      out = temp[0];
      if(temp[1]!=null)
        out += temp[1];
    }
    
    return out;
  } 
  
    

  
  function print_plt2(plt_coord,html,plt_invert,icona)
  {
    var tratto = [];
    //var rows = [], coordinate = [];
    var count = 0;
    var poly = '';
    
    for(var i=0;i<plt_coord.length;i++)
    {
      
      
      var rows = plt_coord[i].split("_");
      for(var j=0;j<rows.length;j++)
      {
        var coord = rows[j].split(",");
        
        var lat = parseFloat(coord[0]);
        var lon = parseFloat(coord[1]);
        
        tratto[count] = new GLatLng(lat ,lon);
        count++;
      }
      
      
        
      
    }
    tratto[0]=tratto[1];
    
    if(plt_invert == 0)
      inizio = createMarker(tratto[0],html,icona);
    else if(plt_invert == 1)
    {
      inizio = createMarker(tratto[tratto.length-1],html,icona);
    }
    
    
    //senellimento del tratto
    var short = [];
    var q=1;
    short[short.length]=tratto[0];
    for(p=0;p<tratto.length;p++)
    {
      if(q==2)
      { 
        short[short.length] = tratto[p];
        q=1;
      }else
        q++;
    }
    short[short.length]=tratto[tratto.length-1];
    //senellimento del tratto
    
    
    
    poly = new GPolyline(short);
    
    //var prova;
    //prova = poly.getVertex(10).lng();
    //alert(prova);
    //alert(prova);
  
    
    out = new plt;
    out.poly = poly;
    out.marker = inizio;
    
    return out;
  }
  
  function reduce_plt(poly,redFact)
  {
    var redPoly = [];
    var interval=1;
    var polyDim = poly.getVertexCount();
    var lat = null,lng = null;
    var out;
    
    //alert(poly.getVertex(0).lat());
    
    redPoly[redPoly.length] = new GLatLng(poly.getVertex(0).lat() ,poly.getVertex(0).lng());
    for(var x=0;x<polyDim;x++)
    {
      
      lat = poly.getVertex(x).lat();
      lng = poly.getVertex(x).lng();
      
      if(interval==redFact)
      { 
        redPoly[redPoly.length] = new GLatLng(lat ,lng);
        interval=1;
      }else
        interval++;
      
    }
    redPoly[redPoly.length] = new GLatLng( poly.getVertex(polyDim-1).lat() ,poly.getVertex(polyDim-1).lng() );
    out = new GPolyline(redPoly);
    
    return out;
  }

//funzione per la stampa di un array
function print_a(array)
{
  
  if(array.length==0)
    alert('array vuoto!');
  else
  {
    var out = '';
    for(var i=0;i<array.length;i++)
    {
      out += ' '+array[i];
    }
    out += ' - ';
    alert(out);
  }
}


/////DEFINISCO LE STRUTTURE DATI GLOBALI//////////////////////////////////////////////////////////

function data()
{
  this.nid;
  
  this.title;
  
  //oggetto che contiene sia marker che altro oggetto plt
  this.obj;
  
  this.poly_bk = null;
  
  this.type;
  
  this.has_child;
  
  this.childs = [];
  
  this.show = true;
  
}

function plt()
{
  this.poly;
  this.marker;
}

function geolist()
{
  this.nid;
  this.title;
  this.term;
  this.show = true;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
    
    

  

  
  var filename = ''; 
  
  var zoomPrev = 5;
   

    // creo la mappa
   var map = new GMap2(document.getElementById("map"));
   map.addControl(new GLargeMapControl3D() );
   map.addControl(new GMapTypeControl());
   
   
   
   map.setCenter( new GLatLng( $("#trigeo-def-lat").val(), $("#trigeo-def-lon").val() ), parseInt($("#trigeo-def-zoom").val()) );
     
   
    
   
   
   var lista = [];
   //array che contiene gli elmenti della schermata corrente
   var memory = [];
   var past = [];

  //questo array Rappresenta i sedici livelli di zoom della mappa
  //e per ogni livello di zoom associa un livello di approssimazione
  //del percorso.
  //Per IE abbassiamo il livello di dettaglio
  if(navigator.appName.indexOf('Microsoft') != -1)
  { 
    var pltRedFact = [0,0,0,0,0,0,0,0,0,0,0,70,30,20,2,1,1,1];
    var browserType = 'IE';
  }
  else 
  {
    var pltRedFact = [0,0,0,0,0,0,0,0,0,0,0,30,25,20,2,1,1,1];
    var browserType = 'Non IE';
  }  
   //var present = [];
   
   
  var url =document.URL;
  var url_p = url.split("/");
  var pos_type = url_p.length-2;
  var pos_id = pos_type+1;
  
  
  //listPath = find_xml_path('read-list.xml');
  //marker_from_rssview_list(listPath);
  //alert(lista.length);
  //create_geoList();
  

  filename = find_xml_path('read.xml',false);
  
  filename +='/45.359865333959746_17.786865234375/40.52215098562377_7.18505859375';
  
  if( url_p[pos_type] == 'node' || url_p[pos_type] == 'term' )  
    filename += '/'+url_p[pos_type]+'/'+url_p[pos_id];
  
  
  marker_from_rssview(filename,false,'/45.359865333959746_17.786865234375/40.52215098562377_7.18505859375');
  
  
  
   
    
  GEvent.addListener(map, "moveend", function() {
    
    //alert(map.getCenter().lat());
    var bounds = map.getBounds();
    var ne = bounds.getNorthEast();
    var sw = bounds.getSouthWest();
    var lat = ne.lat();
    var lon = ne.lng(); 
    var lat1 = sw.lat();
    var lon1 = sw.lng();
    var quadr = lat + '_' + lon + '/' + lat1 + '_' + lon1;
    var zoom = map.getZoom();
    

    
    //stampo sul div pippo le coordinate inquadrate
    //document.getElementById("pippo").innerHTML = quadr + ' ->livello di zoom: '+zoom + ' -> tipo di browser: ' + browserType;
        
    url =document.URL;
    url_p = url.split("/");
    pos_type = url_p.length-2;
    pos_id = pos_type+1;
    
    
  
    filename = find_xml_path('read.xml',false);
    
    filename +='/'+quadr;
    
    if( url_p[pos_type] == 'node' || url_p[pos_type] == 'term' )  
      filename += '/'+url_p[pos_type]+'/'+url_p[pos_id];
    
    
    
    marker_from_rssview(filename,false,quadr);
      
      //alert(cData.length + ' ' + fData.length);
    
      
  });
   
  GEvent.addListener(map, "click", function() {
    //alert('hello');
  });
  
   
    
    
}

else { 
  alert("Sorry, the Google Maps API is not compatible with this browser");
}
  